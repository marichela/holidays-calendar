/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable sort-imports */
import api from "../../api/api"
import Contry from "./country"
import React from "react"

const countryContainer = () => {
  const [selectedCountry, setSelectedCountry] = React.useState("ES")
  const [contries, setContries] = React.useState(null)
  const [isFetching, setIsFetching] = React.useState(true)

  React.useEffect(() => {
    async function fetchContriesList(){
      const contries = await api.getCountries()
      setContries(contries)
      setIsFetching(false)
      return
    }
    fetchContriesList()
    setIsFetching(true)
  }, [setContries])

  function handleChangeCountry(country){
    setSelectedCountry(country)
  }

  return (
    <>
      <Contry
        selectedCountry={selectedCountry}
        contries={contries}
        isFetching={isFetching}
        handleChangeCountry ={(country) =>
          handleChangeCountry(country)}
      />
    </>
  )
}

export default countryContainer
