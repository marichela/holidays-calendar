import CalendarContainer from "../calendar/calendarContainer"
import React from "react"
import style from "../../styles/styles"

const country = (props) => {
  return (
    <>
      {props.isFetching ? null : (
        <style.Select >
          <option value={props.selectedCountry}>{props.selectedCountry}</option>
          {props.contries.map(country => {
            return (
              <option
                value={country.value} key={country.key}
                onClick={() => props.handleChangeCountry(country.key)}
              >
                {country.key}
              </option>)
          })}
        </style.Select>) }
      <CalendarContainer
        selectedCountry={props.selectedCountry}
      />
    </>
  )
}

export default country
