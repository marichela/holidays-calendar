/* eslint-disable sort-imports */
import SelectCountry from "../country/countryContainer"
import React from "react"
import style from "../../styles/styles"

const App = () => {
  return (
    <>
      <style.Container>
        <SelectCountry/>
      </style.Container>
    </>
  )
}

export default App
