/* eslint-disable sort-imports */
import Calendar from "./calendar"
import api from "../../api/api"
import moment from "moment"
import React from "react"

const CalendarContainer = (props) => {
  const [selectedPeriod, setSelectedPeriod] = React.useState([
    moment().year(),
    moment().month()
  ])
  const [selectedHoliday, setSelectedHoliday] = React.useState([])
  const [holidays, setHolidays] = React.useState(null)
  const [isFetching, setIsFetching] = React.useState(true)

  React.useEffect(() => {
    async function fetchHolidays(){
      const holidays = await api.getHolidays(selectedPeriod[0],
        props.selectedCountry)
      setHolidays(holidays)
      setIsFetching(false)
      return
    }
    fetchHolidays()
    setIsFetching(true)
    setSelectedHoliday(null)
  }, [props.selectedCountry, selectedPeriod])

  function handleChangePeriod(event) {
    const action = event.currentTarget.id
    if (action === "left") {
      const newMonth = moment(selectedPeriod).subtract(1, "month")
      return setSelectedPeriod([newMonth.year(), newMonth.month()])
    } else if (action === "right") {
      const newMonth = moment(selectedPeriod).add(1, "month")
      return setSelectedPeriod([newMonth.year(), newMonth.month()])
    }
  }

  function handleClickDay(holiday){
    setSelectedHoliday(holiday)
  }

  return (
    <>
      <Calendar
        selectedPeriod={selectedPeriod}
        handleChangePeriod={handleChangePeriod}
        holidays={holidays}
        isFetching={isFetching}
        selectedHoliday={selectedHoliday}
        handleClickDay={(holiday) =>
          handleClickDay(holiday)
        }
        selectedCountry={props.selectedCountry}
      />

    </>
  )
}

export default CalendarContainer
