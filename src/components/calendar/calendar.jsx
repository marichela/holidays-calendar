import Days from "./days"
import React from "react"
import moment from "moment"
import style from "../../styles/styles"

const Calendar = (props) => {
  return (
    <>
      {props.isFetching ? null : (
        <div>
          <style.Month>
            <style.ArrowLeft id="left" onClick={props.handleChangePeriod} />
            <style.MonthTitle>
              {moment(props.selectedPeriod).format("MMMM YYYY")}
            </style.MonthTitle>
            <style.ArrowRight id="right" onClick={props.handleChangePeriod} />
          </style.Month>
          <Days
            selectedPeriod={props.selectedPeriod}
            holidays={props.holidays}
            selectedHoliday={props.selectedHoliday}
            handleClickDay={(holiday) =>
              props.handleClickDay(holiday)
            }
          />
        </div>)}
    </>
  )
}

export default Calendar
