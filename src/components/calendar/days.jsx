import React from "react"
import style from "../../styles/styles"
import utils from "../../util/util"

const Days = (props) => {
  const selectedYear = props.selectedPeriod[0]
  const selectedMonth = props.selectedPeriod[1] + 1 // has wanted a 0-indexed month
  const monthDaysList = utils.getMonthDaysList(selectedYear, selectedMonth)
  const monthDaysInfo = utils.fillDaysWithInformation(
    utils.getHolidaysPerYear(props.holidays), monthDaysList,
    props.selectedPeriod, selectedMonth)

  return (
    <>
      <style.CalendarGrid>
        {monthDaysInfo.map(day => (
          <div key={day.dayNumber}>
            <style.CalendarDay
              key={day.dayNumber}
              today = {day.today}
              isHoliday = {day.isHoliday}
              onClick = {() => props.handleClickDay(day.holiday)}
            >
              {day.dayNumber}
            </style.CalendarDay>
          </div>
        )
        )}
      </style.CalendarGrid>
      {props.selectedHoliday && (
        props.selectedHoliday.map(holiday => {
          return (
            <style.HolidayName key={holiday.name}>{holiday.name}</style.HolidayName>
          )
        })
      )}

    </>
  )
}
export default Days
