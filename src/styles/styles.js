import styled from "styled-components"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 450px;
`

const Select = styled.select`
  width: 100%;
  height: 35px;
  background: white;
  color: #F08080;
  font-size: 14px;
  border: none;

  option {
    color: black;
    background: white;
    display: flex;
    white-space: pre;
    min-height: 20px;
    padding: 0px 2px 1px;
  }`

const Month = styled.header`
  display: flex;
  justify-content: center;
  height: 50px;
  width: 100%;
  align-items: center;
  background-color: #F08080;
  position: relative;
`

const MonthTitle = styled.h1`
  margin: 0;
  padding: 0;
  font-size: 20px;
  line-height: 70px;
  font-weight: 100;
  color: #fff;
`

const Arrow = styled.div`
  position: absolute;
  width: 0px;
  height: 0px;
  border-style: solid;
  top: 50%;
  margin-top: -7.5px;
  cursor: pointer;
`

const ArrowRight = styled(Arrow)`
  border-width: 7.5px 0 7.5px 10px;
  border-color: transparent transparent transparent #fff;
  right: 20px;
`

const ArrowLeft = styled(Arrow)`
  border-width: 7.5px 10px 7.5px 0;
  border-color: transparent #fff transparent transparent;
  left: 20px;
`

const CalendarGrid = styled.div`
display: grid;
grid-row-gap: 10px;
width: 450px;
grid-template-columns: repeat(7, 1fr);
background-color: #F08080;
`

const CalendarDay = styled.div`
display: flex;
justify-content: center;
align-items: center;
align-self: center;
border-radius: 100%;
width: 40px;
height: 40px;
margin: auto;
cursor: pointer;
color: ${({ isHoliday, today }) => {
    let color = ""
    color = today ? " #F08080" : "#fff" && isHoliday ? " #fff" : "#fff"
    return color
  }};
background-color: ${({ isHoliday, today }) => {
    let color = ""
    color = today ? "#fff" : "#F08080" && isHoliday ? "#FF0000" : "#F08080"
    return color
  }}`

const HolidayName = styled.label`
  font-size: 16px;
  justify-content: center;
  display: grid;
  position: relative;
  line-height: 20px;
  letter-spacing: 1px;
  color: #fff;
`

export default { Container, Month, MonthTitle, Arrow, ArrowRight,
  ArrowLeft, CalendarGrid, CalendarDay, HolidayName, Select }
