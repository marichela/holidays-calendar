import fetch from "isomorphic-unfetch"

/** /////////////////////////////////////////////////////////////////////////////////////////
 //  API
 // Coments: I have had problems wih CROS to make the fetch request, as this test is only for
 // frontend I have added a URL PROXY for CROS
 //////////////////////////////////////////////////////////////////////////////////////////*/

const getHolidays = async (year, country) => {
  try {
    const PROXY_URL = "https://cors-anywhere.herokuapp.com/"
    const testUrl = `https://date.nager.at/api/v2/PublicHolidays/${year}/${country}`

    const res = await fetch(PROXY_URL + testUrl, {
      headers: { "Content-Type": "application/json; charset=utf-8",
        "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "*" } })
    return await res.json()
  } catch (err){
    console.log(err)
  }
}

const getCountries = async () => {
  try {
    const PROXY_URL = "https://cors-anywhere.herokuapp.com/"
    const testUrl = "https://date.nager.at/api/v2/AvailableCountries"
    const res = await fetch(PROXY_URL + testUrl, {
      headers: { "Content-Type": "application/json; charset=utf-8",
        "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "*" } })
    return res.json()
  } catch (err) {
    console.log(err)
  }
}

export default { getHolidays, getCountries }
