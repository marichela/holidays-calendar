/* eslint-disable id-length */
import dayjs from "dayjs"
import moment from "moment"

// ====================================================================================================
const getMonthDays = (year, month) => new Date(year, month, 0).getDate()

// ====================================================================================================
const getDate = (year, month, day) => new Date(year, month, day)

// ====================================================================================================
const formatDate = date => dayjs(date).format("DD")

// ====================================================================================================
const getMonthDaysList = (year, month) => {
  const monthDays = getMonthDays(year, month)
  return Array.from({ length: monthDays }, (_, day) =>
    getDate(year, month - 1, day + 1))
}

// ====================================================================================================
const getHolidaysPerYear = (holidaysList) => holidaysList.map(holiday => {
  return {
    day: moment(holiday.date).date(),
    month: moment(holiday.date).month(),
    name: holiday.localName,
    type: holiday.type
  }
})
// ====================================================================================================
const currentMonth = moment([moment().year(), moment().month()])
const currentDate = moment().format("L")

// ====================================================================================================
const fillDaysWithInformation = (holidays,monthDaysList, selectedPeriod,
  selectedMonth) => {
  let monthDays = []
  monthDaysList.map(day =>
    monthDays.push({
      dayNumber: formatDate(day),
      today: moment(currentMonth).isSame(selectedPeriod) &&
      formatDate(currentDate) === formatDate(day)
        ? true
        : false ,
      isHoliday: false,
      holiday: []
    }))

  const holidaysMonth = holidays.filter(holiday =>
    holiday.month === selectedMonth - 1) // holiday.monthhas wanted a 0-indexed month

  holidaysMonth.map(holiday => {
    return monthDays.map(day => {
      if (holiday.day === parseInt(day.dayNumber,10)) {
        day.isHoliday = true
        day.holiday.push(holiday)
      }
    })
  })
  return monthDays
}

export default { fillDaysWithInformation, getHolidaysPerYear,
  getMonthDays, getMonthDaysList }
